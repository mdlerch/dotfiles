-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
-- Enable VIM help for hotkeys widget when client with matching name is opened:
require("awful.hotkeys_popup.keys.vim")
vicious = require("vicious")
widgets = require("vicious.widgets")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}
-- {{{ Make widgets

-- {{{ Cmus info
-- Initialize widget
-- cmus_widget = wibox.widget.textbox()
-- Register widget
-- vicious.register(cmus_widget, vicious.widgets.cmus,
--     function (widget, args)
--         if args["{status}"] == "Stopped" then 
--             return " - "
--         else
--             return '   '..args["{status}"]..' '.. args["{artist}"]..' - '.. args["{title}"]..'    '
--         end
--     end, 1)
--}}}

-- Memory progressbar
memwidget = awful.widget.progressbar()
memwidget:set_width(10)
-- memwidget:set_height(10)
memwidget:set_vertical(true)
memwidget:set_background_color("#494B4F")
memwidget:set_border_color(nil)
memwidget:set_color({
    type = "linear",
    from = { 0, 0 },
    to = { 10,0 },
    stops = { {0, "#00FF4A"}, {0.5, "#31B758"}, {.9, "#D5F7DF"} }
})
vicious.register(memwidget, vicious.widgets.mem, "$1", 13)

-- Memory text box
memintro = wibox.widget.textbox()
memintro:set_text("Mem: ")

-- CPU graph
cpuwidgetg = awful.widget.graph()
cpuwidgetg:set_width(50)
cpuwidgetg:set_background_color("#494B4F")
cpuwidgetg:set_color("E0E0E0")
vicious.register(cpuwidgetg, vicious.widgets.cpu, "$1")

-- CPU text box
cpuintro = wibox.widget.textbox()
cpuintro:set_text("   Cpu:")

-- Battery progressbar
batwidget = awful.widget.progressbar()
batwidget:set_width(10)
-- batwidget:set_height(14)
batwidget:set_vertical(true)
batwidget:set_background_color("#494B4F")
batwidget:set_border_color(nil)
batwidget:set_color({
    type = "linear",
    from = { 0, 0 },
    to = { 10,0 },
    stops = { {0, "#00BFFF"}, {0.5, "#1DA1CD"}, {.9, "#E1F7FF"} }
})
vicious.register(batwidget, vicious.widgets.bat, "$2", 30, "BAT0")

-- Battery text box
batintro = wibox.widget.textbox()
vicious.register(batintro, vicious.widgets.bat, "$3", 30, "BAT0")

-- Need some extra space
batspace = wibox.widget.textbox()
batspace:set_text("   ")

-- Temperature text box
tempwidget = wibox.widget.textbox()
if host == "buddy" then
    vicious.register(tempwidget, vicious.widgets.therm2, " $1C  ", 20, {"hwmon1", "hwmon"} )
else
    vicious.register(tempwidget, vicious.widgets.thermal, " $1C  ", 20, {"thermal_zone0", "sys"} )
end

-- Clock text box
mytextclock = awful.widget.textclock(" %a %b %d, %I:%M ", 20)

--- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
-- beautiful.init("/usr/share/awesome/themes/default/theme.lua")
beautiful.init("/home/mike/.config/awesome/theme.lua")

-- This is used later as the default terminal and editor to run.
-- terminal = "xterm"
-- editor = os.getenv("EDITOR") or "nano"
-- editor_cmd = terminal .. " -e " .. editor
terminal = "roxterm"
terminaltmux = "/home/mike/bin/termattach"
terminalmusic = "/home/mike/bin/termmusic"
terminalnotmux = "roxterm"
editor = os.getenv("EDITOR") or "nvim"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminalnotmux .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
-- modkey = "Mod4"
WK = "Mod4"
AK = "Mod1"
CK = "Control"
SK = "Shift"

-- Table of layouts to cover with awful.layout.inc, order matters.
-- awful.layout.layouts = {
--     awful.layout.suit.floating,
--     awful.layout.suit.tile,
--     awful.layout.suit.tile.left,
--     awful.layout.suit.tile.bottom,
--     awful.layout.suit.tile.top,
--     awful.layout.suit.fair,
--     awful.layout.suit.fair.horizontal,
--     awful.layout.suit.spiral,
--     awful.layout.suit.spiral.dwindle,
--     awful.layout.suit.max,
--     awful.layout.suit.max.fullscreen,
--     awful.layout.suit.magnifier,
--     awful.layout.suit.corner.nw,
--     -- awful.layout.suit.corner.ne,
--     -- awful.layout.suit.corner.sw,
--     -- awful.layout.suit.corner.se,
-- }
awful.layout.layouts = {
    awful.layout.suit.tile,
--     awful.layout.suit.tile.left,
--     awful.layout.suit.tile.bottom,
--     awful.layout.suit.tile.top,
    awful.layout.suit.fair,
--     awful.layout.suit.fair.horizontal,
--     awful.layout.suit.spiral,
--     awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.floating,
--     awful.layout.suit.max.fullscreen,
--     awful.layout.suit.magnifier,
--     awful.layout.suit.corner.nw,
--     -- awful.layout.suit.corner.ne,
--     -- awful.layout.suit.corner.sw,
--     -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ WK }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ WK }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    -- awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    -- awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                    awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end)
                )
local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() and c.first_tag then
                                                      c.first_tag:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, client_menu_toggle_fn()),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mylayoutbox,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            wibox.widget.systray(),
            cmus_widget,
            memintro,
            memwidget,
            batspace,
            batintro,
            batwidget,
            cpuintro,
            cpuwidgetg,
            tempwidget,
            mytextclock,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({CK, AK}, "Left",  awful.tag.viewprev),
    awful.key({CK, AK}, "Right", awful.tag.viewnext),
    awful.key({CK, AK}, "Escape", awful.tag.history.restore),
    awful.key({CK, AK}, "space", function() awful.screen.focus_relative(1) end),

    -- Moving around clients
    awful.key({WK}, "j", function()
        awful.client.focus.byidx( 1)
        if client.focus then client.focus:raise() end
    end),
    awful.key({WK}, "k", function()
        awful.client.focus.byidx(-1)
        if client.focus then client.focus:raise() end
    end),
    awful.key({AK}, "Tab", function()
        awful.client.focus.byidx(1)
        if client.focus then
            client.focus:raise()
        end
    end),
    awful.key({SK, AK}, "Tab", function()
        awful.client.focus.byidx(-1)
        if client.focus then
            client.focus:raise()
        end
    end),

    -- Layout manipulation
    awful.key({WK, AK}, "j", function() awful.client.swap.byidx(  1) end),
    awful.key({WK, AK}, "k", function() awful.client.swap.byidx( -1) end),
    awful.key({WK, AK}, "l", function() awful.tag.incmwfact( 0.05)   end),
    awful.key({WK, AK}, "h", function() awful.tag.incmwfact(-0.05)   end),
    awful.key({WK, SK}, "h", function() awful.tag.incnmaster( 1)     end),
    awful.key({WK, SK}, "l", function() awful.tag.incnmaster(-1)     end),
    awful.key({WK, CK, AK}, "h", function() awful.tag.incncol( 1)   end),
    awful.key({WK, CK, AK}, "l", function() awful.tag.incncol(-1)   end),
    awful.key({WK, CK, AK},     "space", function() awful.layout.inc(1, mouse.screen) end),
    awful.key({WK, AK, CK, SK}, "space", function() awful.layout.inc(-1, mouse.screen) end),

    -- Launch Programs
    awful.key({CK, AK}, "t", function() awful.util.spawn(terminalnotmux) end),
    awful.key({CK, AK}, "e", function() awful.util.spawn(terminalnotmux) end),
    awful.key({CK, AK}, "a", function() awful.util.spawn(terminaltmux) end),
    awful.key({CK, AK}, "f", function() awful.util.spawn("chromium") end),
    awful.key({CK, AK}, "d", function() awful.util.spawn("darktable --noiseprofiles /home/mike/dotfiles/noiseprofiles.json") end),
    awful.key({CK, AK}, "g", function() awful.util.spawn("google-chrome") end),
    awful.key({CK, AK}, "c", function() awful.util.spawn("xcalc") end),
    awful.key({CK, AK}, "m", function() awful.util.spawn("mendeleydesktop") end),

    awful.key({}, "XF86Calculator", function () awful.util.spawn("xcalc") end),

    -- Media
    awful.key({CK, WK}, "p", function () awful.util.spawn_with_shell("playertoggle") end),
    awful.key({CK, WK}, "n", function () awful.util.spawn_with_shell("playernext") end),
    awful.key({CK, WK}, "b", function () awful.util.spawn_with_shell("playerprev") end),
    awful.key({CK, WK}, "l", function () awful.util.spawn_with_shell("volinc") end),
    awful.key({CK, WK}, "k", function () awful.util.spawn_with_shell("voldec") end),
    awful.key({CK, WK}, "m", function () awful.util.spawn_with_shell("volmute") end),

    awful.key({}, "XF86AudioPlay", function () awful.util.spawn_with_shell("playertoggle") end),
    awful.key({}, "XF86AudioNext", function () awful.util.spawn_with_shell("playernext") end),
    awful.key({}, "XF86AudioPrev", function () awful.util.spawn_with_shell("playerprev") end),
    awful.key({}, "XF86AudioRaiseVolume", function () awful.util.spawn_with_shell("volinc") end),
    awful.key({}, "XF86AudioLowerVolume", function () awful.util.spawn_with_shell("voldec") end),
    awful.key({}, "XF86AudioMute", function () awful.util.spawn_with_shell("volmute") end),

    -- Switch page up/page down and web forward back
    awful.key({WK}, "Return", function () awful.util.spawn_with_shell("pgup") end),
    awful.key({WK, SK}, "Return", function () awful.util.spawn_with_shell("pgdown") end),

    -- Awesome commands
    awful.key({WK, CK, AK}, "r", awesome.restart),
    awful.key({WK, CK, AK}, "q", awesome.quit),
    awful.key({WK, CK, AK}, "n", function() wp_new() end),
    awful.key({WK, CK, AK}, "m", xrandr),

    -- Menubar
    awful.key({ CK, AK }, "r", function() menubar.show() end)
)

clientkeys = awful.util.table.join(
    awful.key({WK, AK}, "c", function(c) c.kill(c) end),
    awful.key({WK, AK}, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({WK}, "F11", function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({WK}, "f", awful.client.floating.toggle),
    awful.key({WK}, "o", awful.client.movetoscreen),
    awful.key({WK}, "t", function (c) c.ontop = not c.ontop            end),
    awful.key({WK}, "m", function (c)
        c.maximized_horizontal = not c.maximized_horizontal
        c.maximized_vertical   = not c.maximized_vertical
    end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ CK }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag.
        awful.key({ CK, AK }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ CK, SK }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.movetotag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Move client to other screen
        awful.key({ CK, SK, AK }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local cs = mouse.screen.index
                          for s in screen do
                              if not (s.index == cs) then
                                  local mt = s
                                  local tag = awful.tag.gettags(mt)[i]
                                  awful.client.movetotag(tag)
                              end
                          end

                          -- if cs.index == 1 then
                          --     local mt = 2
                          -- else
                          --     local mt = 1
                          -- end
                          -- local tag = awful.tag.gettags(mt)[i]
                          -- if tag then
                          --     awful.client.movetotag(tag)
                          -- end
                          -- for s in screen do
                          --     local cs = mouse.screen
                          --     t
                          --     if not (cs.index == s.index) then
                          --         local tag = awful.tag.gettags(cs)[i]
                          --         if tag then
                          --             awful.client.movetotag(tag)
                          --         end
                          --         break
                          --     end
                          --  end
                      end
                  end,
                  {description = "move focused client to tag of other screen #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ AK }, 1, awful.mouse.client.move),
    awful.button({ AK }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     screen = awful.screen.focused,
                     buttons = clientbuttons } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    {
        rule = { name = "CRAN mirror" },
        properties = { floating = true }
    },
    {
        rule = { class = "XCalc" },
        properties = { floating = true }
    },
    {
        rule = { class = "R_x11" },
        properties = {floating = true, ontop = true },
    },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    -- Set Firefox to always map on tags number 2 of screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { tag = tags[1][2] } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

os.execute("/home/mike/bin/run_nmapp &")
-- os.execute("wicd &")
-- os.execute("/home/mike/bin/myconky")
-- }}}
